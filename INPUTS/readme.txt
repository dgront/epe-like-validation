ligand_deposit_list1ring.txt - contains list of PDB deposits' ids (PDBid) that contain EPE or MES
hepes_mes_rcsb_statistics.tsv - contains information about deposition date, release date, resolution, PQ1, R_fre, Rwork, R_obs, RSR for each EPE or MES instance in PDB files presented in ligand_deposit_list1ring.txt
hepes_mes_list.tsv - contains list of PDBid, ligand code, chain and residue number for PDB deposits that were solved using X-ray diffrationa and re not PanDDA structures.

