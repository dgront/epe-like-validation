Direcotires:
CSD - contains results of process_ligand.py performed on CSD deposits
ideal - contains results of process_ligand.py performed on ideal structures from Ligand Expo and QM calculations
EPE-like - contains results of process_ligand.py performed on PDB deposits
PDB_REDO - contains results of process_ligand.py performed on PDB_REDO deposits
HKL_refine - contains HKL reports for re-refinements performed on selected deposits
