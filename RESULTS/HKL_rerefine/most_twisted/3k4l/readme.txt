hkl_refine_3 - initial input after two runs of 0 cycles of refinement
hkl_refine_4 - only MES was re-paired in coot
hkl_refine_7 - MES and some aa re-paired in coot
hkl_refine_8 - 10 cycles of refinement on hkl_refine_3
hkl_refine_9 - 10 cycles of refinement on input structure
hkl_refine-10-12 - manual removal of clashes, MES was not moved
hkl_refine_13 - MES 902 A and B removed, nothing placed there
hkl_refine_14 - CMB suggestions (SO4) placed in the place of MES 902 A and B, initial.mtz
hkl_refine15 - CMB suggestions (SO4) placed in the place of MES 902 A and B, hklrefine_13.mtz used together with hkl_refine_13-coot-0.pdb

After checking with David it seems that MES is the correct ligand so from now next refinements will be based on hkl_refine_12 ->hkl_refine_16

hkl_refine_21 - I think thi is the best one, one water was removed and some manual refinement was done. As input hkl_refine_17-coot-2; next structures have terminal aminoacids added
hkl_refine_22 - residue 45 added to chain A and 45 with 44 to chain B. Input is hkl_refine_17-coot-3
hkl_refine_23 - refined with tls from hkl_refine_17-coot-3
hkl_refine_24 - 10 more cycles of refinement with TLS, no manual corrections;
hkl_refine_25 - 10 more cycles of refinement on hkl_refine_20 to check if the distance between HIS and FAD will increase

Final structure - hkl_refine_33 - prepared from hkl_refine_17-coot-3.pdb and hkl_refine_13.mtz with NCS and TLS
186A should have alternative?

Next structures (34 , and then +10 ref cycles as 35) have removed side-chains where the density was missing. hkl_refine_17-coot-3 and 13 mtz was used

in build_model_3 are refinements of only ligand (hkl_refine_41 and above)
hkl_refine_42 - MES 902 B changed to ideal chair and then manually, with fixed ring, refined to bent into the density, and then refined with restrains from Michał script (0.1 A around ligand)

