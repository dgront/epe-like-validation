hkl_refine_4 - refinement with only ligand mobile, other residues fixed
hkl_refine_5 - ligand manually changed to ideal boat, other residues restrained
hkl_refine_6 - no restrains
hkl_refine_7 - refinement with no restrains of hkl_refine_5-coot which was manually corrected before run without restreins and it was not a good idea 
hkl_refine_8 - some manual corrections done
hkl_refine_9 - some more manual corrections done

hkl_refine_493 - initial model with anisou after 0 cycles of refinement
hkl_refine_494 - local lig ref - till boat is flat, so in next input to hkl_refine_5 will be used, still local ref
hkl_refine_496 - manual boat. local ref
hkl_refine_498 - final structure with anisoi
hkl_refine_501 - trully finbal, one PEG close to second MES removed since had bad aniso and didn't match to the density.

build_model_3 - MES removed, GOL added 
