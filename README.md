# EPE-like ring analysis


EPE and MES, that are frequent components of crystallization and purifications buffers. Here we provide bioinformatic tools for analysis of a six-membered aliphatic ring in these ligands, such as geaometry or different structure quality metrics.
Scripts for search for homologous structures to identify other small molecules that could bind in place of the EPE or MES are also provided.


Please remember to provide correct path to `data` directory (can be downloaded from [here](http://bioshell.pl/downloads/bioshell/data.tar.gz)) and to `pybioshell` library (can be downloaded from [here](https://bioshell.pl/~jmacnar/pybioshell.so)) in all python scripts.



For more details, read our manuscript:

[Macnar, J. M., Brzezinski, D., Chruszcz, M., & Gront, D. (2022). Analysis of protein structures containing HEPES and MES molecules. Protein Science, 31(9), e4415.](https://doi.org/10.1002/pro.4415)


Easy to use implementation of the six-membered-ring analysis is provided as [Google Colab notebook](https://colab.research.google.com/github/JMacnar/ring_analysis/blob/master/sixmembered_rings.ipynb)

This work has been supported by Polish National Science Centre (Grant No. 2019/35/N/ST6/04459)
