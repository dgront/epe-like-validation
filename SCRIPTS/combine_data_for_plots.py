import sys
import re
from pathlib import Path
import os


if __name__ == '__main__':
    filename1 = sys.argv[1]
    filename2 = sys.argv[2]
    out_file = Path("./angles_statistics.tsv")
    if out_file.exists():
        print('The file ' + str(out_file) + ' already exists. Removed')
        os.remove(out_file)
        with open(out_file, "w") as output_file:
            print("Filename", "Chain", "Residue_number", "Ligand", "First_wing_angle", "Second_wing_angle",
                  "Twist_angle", "Twist_angle_err",
                  "Conformation", "t1", "t2", "t3", "Bond1", "Bond2", "Bond3", "Bond4", "Bond5", "Bond6",
                  "Avg_bonds_err", "B-factor_min", "B-factor_max", "B-factor_avg", "PDBid", "Ligand_code", "Deposition_date", "Modification_date", "Method", "Resolution", "ls_R_factor_R_free", "ls_R_factor_R_work", "ls_R_factor_obs",
                  file=output_file)
    else:
        with open(out_file, "w") as output_file:
            print("Filename", "Chain", "Residue_number", "Ligand", "First_wing_angle", "Second_wing_angle",
                  "Twist_angle", "Twist_angle_err",
                  "Conformation", "t1", "t2", "t3", "Bond1", "Bond2", "Bond3", "Bond4", "Bond5", "Bond6",
                  "Avg_bonds_err","B-factor_min", "B-factor_max", "B-factor_avg", "PDBid", "Ligand_code", "Deposition_date", "Modification_date", "Method", "Resolution", "ls_R_factor_R_free", "ls_R_factor_R_work", "ls_R_factor_obs",
                  file=output_file)

    with open(filename1, 'r') as f:
        next(f)
        for line_f in f:
            angles_file = re.split("\s|\-", line_f)
            p = re.compile((angles_file[3])[:-4])
            r = re.compile(angles_file[0])
            with open(filename2, 'r') as g:
                for line_g in g:
                    result2 = p.search(line_g)
                    if result2:
                        result1 = r.search(line_g)
                        if result1:
                            with open(out_file, 'a') as o:
                                print("{} {} ".format(
                                    line_f.strip(), line_g.strip()), file=o)
                        else:
                            continue
                    else:
                        continue
