import sys, os
from os import path
sys.path.append("/home/jmacnar/src.git/bioshell/bin")
#os.environ["BIOSHELL_DATA_DIR"] = '/home/jmacnar/src.git/bioshell/data'
from pybioshell.core.alignment import SequenceSemiglobalAligner, sum_identical
from pybioshell.core.data.io import Pdb
from pybioshell.std import vector_core_data_basic_Vec3
from pybioshell.core.calc.structural.transformations import *
from extract_ligands_from_pdb import download_pdb

DISTANCE_CUTOFF = 5.0 # --- in Angstroms
SEQ_ID_CUTOFF   = 0.2 # --- sequence identity fraction
LIGAND_CUTOFF   = 5.0

if len(sys.argv) < 2 :
  print("""

Computes pairwise sequence alignment between two chains and locates a ligand
that substitutes EPE or MES residue


USAGE:
    python3 superimpose_for_ligand.py


EXAMPLE:
    python3 superimpose_for_ligand.py 1ajkA.txt
  """)
  sys.exit()


class ProteinData:
    def __init__(self, pdb_code, chain_code):
       # pdb_file = "/mnt/storage/DATABASES/PDB_MIRROR/wwpdb/pdb%s.ent.gz" % pdb_code
       # if not path.exists(pdb_file):
       #     print("not in pdb mirror", pdb_file)
        download_pdb(pdb_code)
        pdb_file = pdb_code + ".pdb"
        self.__strctr = Pdb(pdb_file,"", True).create_structure(0)
        self.__chain = self.__strctr.get_chain(chain_code)
        self.__sequence = self.__chain.create_sequence()
        self.__ligands = []
        os.remove(pdb_file)
    @property
    def sequence(self): return self.__sequence

    @property
    def code(self): return self.__strctr.code()

    @property
    def chain_code(self): return self.__chain.id()

    @property
    def chain(self): return self.__chain

    @property
    def ligands(self): return self.__ligands

    def find_ligands(self, *ligand_code3):
        """Finds ligands by name. If the list is empty, finds any ligand"""
        self.__ligands = []
        for ir in range(self.__chain.terminal_residue_index() + 1, self.__chain.size()):
            resid = self.__chain[ir]
            code3 = resid.residue_type().code3
            if len(ligand_code3) == 0:
                if code3 != "HOH":   # Skip water molecules, they are so obvious and abundant
                    self.__ligands.append(resid)
            else:
                if code3 in ligand_code3:
                    self.__ligands.append(resid)
        return self.__ligands


class ProteinAlignment:
    def __init__(self, host_protein: ProteinData, homolog_protein: ProteinData):
        self.__host = host_protein
        self.__hmlg = homolog_protein

        # ---------- calculate a semiglobal alignment
        aligner = SequenceSemiglobalAligner(max(self.__host.sequence.length(), self.__hmlg.sequence.length()))
        self.__score = aligner.align(self.__host.sequence, self.__hmlg.sequence, -10, -1, "BLOSUM62")
        alignment = aligner.backtrace_sequence_alignment()
        self.__aligned_host = alignment.get_aligned_query()     # --- it's a string
        self.__aligned_hmlg = alignment.get_aligned_template()  # --- also a string
        # ---------- calculate identity fraction
        self.__identity = sum_identical(self.__aligned_host, self.__aligned_hmlg)
        self.__identity /= min(self.__host.sequence.length(), self.__hmlg.sequence.length())

        # --- collect residue pairs as they appear in the alignment (ommit gaps in any of the two sequences)
        i_lig, i_hom = 0, 0
        self.__aligned_residue_pairs = []
        for ir in range(len(self.__aligned_host)):
            if self.__aligned_host[ir] != '-': i_lig += 1
            if self.__aligned_hmlg[ir] != '-': i_hom += 1
            if self.__aligned_host[ir] != '-' and self.__aligned_hmlg[ir] != '-':
                self.__aligned_residue_pairs.append((self.__host.chain[i_lig], self.__hmlg.chain[i_hom]))

    @property
    def aligned_residue_pairs(self): return self.__aligned_residue_pairs

    @property
    def identity(self): return self.__identity

    def aligned_pocket_residues(self, ligand_resid, protein_index: int, distance_cutoff):
        """Extracts these residue pairs that are close to a given ligand"""
        pocket_pairs = []
        for pair in self.__aligned_residue_pairs:
            if pair[protein_index].min_distance(ligand_resid,distance_cutoff) < distance_cutoff:
                pocket_pairs.append(pair)
        return pocket_pairs


def pocket_identity(pocket_residue_pairs):
    cnt = 0
    for q, t in pocket_residue_pairs:
        if q.residue_type().code3 == t.residue_type().code3: cnt += 1
    return cnt


def superimpose(residue_pairs):
    query = vector_core_data_basic_Vec3()
    tmplt = vector_core_data_basic_Vec3()
    n_superimposed = 0
    for q, t in residue_pairs:
        try:
            qca = q.find_atom(" CA ")
            tca = t.find_atom(" CA ")
            query.append(qca)
            tmplt.append(tca)
            n_superimposed += 1
        except:
            print("WARING: missing CA either in", q, "or in", t, file=sys.stderr)
            for ai in range(q.count_atoms()): print(q[ai], file=sys.stderr)
            for ai in range(t.count_atoms()): print(t[ai], file=sys.stderr)

    if n_superimposed < 3:
        print("ERROR: not enough atoms to superimpose!",  file=sys.stderr)
        return None

    rms = CrmsdOnVec3()
    crmsd_val = rms.crmsd(query, tmplt, n_superimposed, True)

    return crmsd_val, rms


def rotate_residues(residues_list, rt):
    for r in residues_list:
        for ai in range(r.count_atoms()):
            rt.apply_inverse(r[ai])


def print_residues(resids_list, **kwargs):
    out_file = kwargs.get("file", sys.stdout)
    for r in resids_list:
        for ai in range(r.count_atoms()):
            print(r[ai].to_pdb_line(), file=out_file)

if __name__ == "__main__":

    fname = sys.argv[1]
    reference_protein = ProteinData(fname[0:4], fname[4])      # --- pdb_fname, chain_code
    reference_protein.find_ligands("EPE", "MES")                         # --- Find the ligand: EPE or MES in our case

    if len(reference_protein.ligands) == 0:
        print("ERROR: the ligand-chain has no EPE or MES ligand", file=sys.stderr)
        sys.exit(1)
        
    for line in open(fname):
        try:
            homolog_protein = ProteinData(line[0:4], line[4])
        except Exception as Error:
            print("ERROR!", Error, file=sys.stderr)
            continue
        homolog_ligands = homolog_protein.find_ligands()
        if len(homolog_ligands) == 0:
            continue
        aligned = ProteinAlignment(reference_protein, homolog_protein)
        if aligned.identity < SEQ_ID_CUTOFF:
            continue

        for ligand in reference_protein.ligands:
            pocket = aligned.aligned_pocket_residues(ligand, 0, DISTANCE_CUTOFF)
            if len(pocket) < 3 : 
                print("Pocket too small for:",reference_protein.code, homolog_protein.code, file=sys.stderr)
                continue
            homolog_ligands_in_pocket = []
            for h_ligand in homolog_ligands:
                for pair in aligned.aligned_residue_pairs:
                    if h_ligand.min_distance(pair[1], DISTANCE_CUTOFF) < DISTANCE_CUTOFF:
                        homolog_ligands_in_pocket.append(h_ligand)
                        break
            if len(homolog_ligands_in_pocket) == 0:
                continue

            # ---------- find the rototranslation (rt)
            try:
                crmsd, rt = superimpose(pocket)
            except Exception as error:
                print("ERROR!", error, file=sys.stderr)
                continue
            # --- transform ligands from homologus chain
            rotate_residues(homolog_ligands_in_pocket, rt)
            # --- find ligands that match EPE or MES, prepare output
            matching_ligands = ""
            for h_ligand in homolog_ligands_in_pocket:
                d = h_ligand.min_distance(ligand, LIGAND_CUTOFF)
                if d < LIGAND_CUTOFF:
                    matching_ligands += str(h_ligand) + " %5.3f" % d

            matching_ligands = "NO LIGANDS" if len(matching_ligands) == 0 else matching_ligands
            # --- Print a report line
            print("%c %s : %s %c %5.3f %2d %6.3f %2d %s" % (reference_protein.chain_code, str(ligand),
                  homolog_protein.code, homolog_protein.chain_code,
                    aligned.identity, pocket_identity(pocket), crmsd, len(pocket), matching_ligands))

            # --- Print result in the PDB format - only if a ligand has been found
            if matching_ligands != "NO LIGANDS":
                fname = "%4s%c_%3s%d_%4s%c.pdb" % (reference_protein.code, reference_protein.chain_code,
                                                   ligand.residue_type().code3, ligand.id(), homolog_protein.code,
                                                   homolog_protein.chain_code)
                out_file = open(fname, "w")
                print("MODEL     1", file=out_file)
                print_residues([pair[0] for pair in pocket], file=out_file)
                print_residues([ligand], file=out_file)
                print("ENDMDL", file=out_file)
                print("MODEL     2", file=out_file)
                rotate_residues([pair[1] for pair in pocket], rt)
                print_residues([pair[1] for pair in pocket], file=out_file)
                print_residues(homolog_ligands_in_pocket, rt=rt, file=out_file)
                print("ENDMDL", file=out_file)
                out_file.close()
