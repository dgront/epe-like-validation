import sys

if __name__ == '__main__':
    fname1 = 'hepes_mes_list.tsv'
    fname2 = 'OUTsort-fixed'
    fname3 = 'OUTsdata__input40'

    """
    Prepare a fasta dictionary:
    https://stackoverflow.com/questions/32596432/search-fasta-file-in-python-return-read-efficiently
    """
    with open(fname2, 'rb') as fp:
        lines = fp.read().splitlines()

    fastaDict = {}

    # Just to start populating the dictionary later
    fastaName = 'dummy'
    fastaSeq = ''
    for line in lines:
        if len(line) == 0:
            continue
        elif line.startswith(b'>') == True:
            fastaDict.update({fastaName: fastaSeq})
            fastaName = line[1:]
            fastaSeq = ''
        else:
            fastaSeq = line

    fastaDict.update({fastaName: fastaSeq})  # Putting the values from the last loop
    fastaDict.pop('dummy')  # Now removing the dummy

    """
    Read file with list of hepes/mes deposits 
    """

    with open(fname1) as f:
        next(f)
        for line1 in f:
            pdbid = line1.strip('\"').split('" "')
            pdb_pattern = bytes(pdbid[0] + pdbid[2].strip("\'"), encoding='utf8')
            results = [key for key, value in fastaDict.items() if pdb_pattern in key] # check if given pdbid is in the fasta dictonary
            #result_value = [value for key, value in geneDict.items() if pattern in key]
            homologs = results.copy()
            print(pdb_pattern, len(results), file=sys.stderr)
            try:
                cluster_pdbid = bytes(results[0].split()[0])
            except Exception as error:
                print("Some error occured:", error, file=sys.stderr)
                continue
            with open(fname3, 'rb') as g:
                next(g)
                for line3 in g:
                    if cluster_pdbid in line3: # find homologous structures in the similarity file
                        pdbids_to_add = line3.strip().split()
                        homologs.append(pdbids_to_add[0])
                        homologs.append(pdbids_to_add[1])
                        homologs_from_fastaDict = [key for key, value in fastaDict.items() if pdbids_to_add[0] in key or pdbids_to_add[1] in key]
                        for i in homologs_from_fastaDict:
                            j = i.decode("utf-8").strip().split()
                            for k in j:
                                homologs.append(bytes(k, "utf-8"))
                        homologs = list(set(homologs))
                homologs_list = [set(homologs)]
                #new_key = b' '.join(set(homologs))
                #old_key = results[0]
                #fastaDict[new_key] = fastaDict[old_key]
                #del fastaDict[old_key]
            with open(pdb_pattern.decode("utf-8") + '.txt', "w") as out:
                for keys in homologs_list:
                #for key, value in fastaDict.items():
                    for key in keys:
                        print(key.decode("utf-8"), file=out)
