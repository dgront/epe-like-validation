import sys, requests, os
from os import path

sys.path.append("/home/asia/src.git/epe-like-validation")
sys.path.append("/home/asia/src.git/bioshell/bin")  # update the path to the `pybioshell.so` location, can be downloaded from https://bioshell.pl/~jmacnar/pybioshell.so
os.environ["BIOSHELL_DATA_DIR"] = '/home/asia/src.git/bioshell/data'  # update the path to bioshell's `data` directory, can be downloaded from http://bioshell.pl/downloads/bioshell/data.tar.gz
import gzip
from pybioshell.core.data.io import Pdb
from pybioshell.core.data.structural import Residue


def extract_ligand(pdb_file_name, ligand_name, cutoff_distance, flaga):
    """ flaga = mirror - file from pdb mirror
        flaga = rcsb - file from rscb.org"""
    if os.stat(pdb_file_name).st_size == 0:
        print(pdb_file_name, "has", os.stat(pdb_file_name).st_size)
        return
    s = Pdb(str(pdb_file_name), "is_not_hydrogen is_not_alternative", False, False).create_structure(0)
    ligands = []
    for ic in range(s.count_chains()):
        chain = s[ic]
        for ir in range(chain.count_residues()):
            if chain[ir].residue_type().code3 == ligand_name:
                ligands.append(chain[ir])
    for l in ligands:
        fname = "%s-%d-%s-%s.pdb" % (l.residue_type().code3, l.id(), l.owner().id(), s.code())
        fout = open(fname, "w")
        for ic in range(s.count_chains()):
            chain = s[ic]
            for ir in range(chain.count_residues()):
                r = chain[ir]
                if r.min_distance(l) < cutoff_distance:
                    if r.id() == l.id():
                        for ai in range(r.count_atoms()):
                            fout.write(r[ai].to_pdb_line() + "\n")
                    if r.residue_type().code3 != l.residue_type().code3:
                        for ai in range(r.count_atoms()):
                            fout.write(r[ai].to_pdb_line() + "\n")
        if flaga == "mirror":
            with gzip.open(pdb_file_name, 'rt') as f:
                for line in f:
                    if line.startswith("CONECT"):
                        fout.write(line)
        elif flaga == "rcsb":
            for line in open(pdb_file_name).readlines():
                if line.startswith("CONECT"):
                    fout.write(line)

        fout.close()


def remove_chars(inpt, which_ones='",()'):
    for c in list(which_ones):
        inpt = inpt.replace(c, "")
    return inpt


def download_pdb(pdb_code):
    try:
        r = requests.get("https://files.rcsb.org/view/%s.pdb" % pdb_code, allow_redirects=True)
        #r = requests.get("https://pdb-redo.eu/db/%s/%s_final_tot.pdb" % (pdb_code,pdb_code), allow_redirects=True)
        
        open(pdb_code + ".pdb", "wb").write(r.content)
    except Exception as error:
        print("An issue occure during", pdb_code, "downloading", error)


def process_list_file(fname):
    with open(fname) as f:
        next(f)
        for line in f:
            tokens = remove_chars(line).strip().split()
            pdb_mirror = "/mnt/storage/DATABASES/PDB_MIRROR/wwpdb/pdb%s.ent.gz" % tokens[0]
            if not path.exists(pdb_mirror):
                print("not in pdb mirror", tokens[0])
                download_pdb(tokens[0])
                fname = tokens[0] + ".pdb"
                extract_ligand(fname, tokens[1], 5.0, "rcsb")
                os.remove(fname)
            else:
                try:
                    extract_ligand(str(pdb_mirror), tokens[1], 5.0, "mirror")
                except Exception as error:
                    print("Problem with pdb file from mirror", tokens[0], error)
                    download_pdb(tokens[0])
                    fname = tokens[0] + ".pdb"
                    extract_ligand(fname, tokens[1], 5.0, "rcsb")
                    os.remove(fname)


if __name__ == "__main__":

    if len(sys.argv) == 3:
        code = sys.argv[1]
        fname = code + ".pdb"
        pdb_mirror = "/mnt/storage/DATABASES/PDB_MIRROR/wwpdb/pdb%s.ent.gz" % code
        if not path.exists(pdb_mirror):
            print("not in pdb mirror", code)
            download_pdb(code)
            fname = tokens[1] + ".pdb"
            extract_ligand(fname, tokens[0], 5.0, "rscb")
            os.remove(fname)
        else:
            extract_ligand(pdb_mirror, sys.argv[2], 5.0, "mirror")
    #        os.remove(fname)
    elif len(sys.argv) == 2:
        process_list_file(sys.argv[1])
